# Acamica Sprint project 1

Este es el proyecto de acamica para el sprint 1

## Installation

Descargar el repositorio e instalar los package del proyecto como se muestra a continuacion

```bash
npm install
```

## Uso

* Ejecutar el proyecto con el siguiente comando
```bash
node src/index.js
```
* Entrar en la documentacion de swagger en la url: [Documentation](http://localhost:3000/api-docs/#/)

* El usuario administrador es Mauricio y la contraseña 12345

* Existe un array de productos predefinidos como lo muestro a continuación

```bash
 [
    {
        id: 1,
        nombre: "Cocacola",
        precio: 3000,
        descripcion: "bebiba gaseosa",

    },
    {
        id: 2,
        nombre: "Pony Malta",
        precio: 2500,
        descripcion: "bebiba gaseosa"
    },
    {
        id: 3,
        nombre: "Camisa",
        precio: 30000,
        descripcion: "Camisa manga larga negra"
    }
]
```

* Asegurate de usar Basic-Auth para la autenticación.
